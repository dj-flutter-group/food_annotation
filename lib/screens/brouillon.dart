/*import 'package:flutter/material.dart';
import 'package:food_annotation/config/config.dart';

class CButton extends StatelessWidget {
  VoidCallback onPressed;
  Color? buttonColor;
  double? borderRadius;
  Color? textColor;
  IconData icon;
  String buttonText;
  Color? borderColor;

  CButton({
    required this.onPressed,
    this.borderRadius,
    this.borderColor,
    this.buttonColor,
    this.textColor,
    required this.icon,
    required this.buttonText,
    Key? key,
  }) : super(key: key);

  @override
  /*Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.symmetric(horizontal: 30, vertical: 15),
        decoration: BoxDecoration(
            color: buttonColor ?? Config.colors.primaryColor,
            border: Border.all(
              color: borderColor ?? Config.colors.secondColor,
            ),
            borderRadius: BorderRadius.circular(borderRadius ?? 0)),
        /*child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: Colors.transparent,
            onPrimary: Colors.transparent,
          ),
          onPressed: onPressed,
          child: Row(
            children: [
              Text(
                buttonText,
                style: TextStyle(
                    color: textColor,
                    fontSize: 25,
                    fontWeight: FontWeight.w400),
              ),
              Icon(icon),
            ],
          ),
        )*/);
  }
}*/

  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 30, vertical: 15),
      decoration: BoxDecoration(
          color: buttonColor ?? Config.colors.primaryColor,
          border: Border.all(
            color: borderColor ?? Config.colors.secondColor,
          ),
          borderRadius: BorderRadius.circular(borderRadius ?? 0)),
      child: Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: onPressed,
            child: Row(
              children: [
                Text(
                  buttonText,
                  style: TextStyle(
                    
                      color: textColor,
                      fontSize: 25,
                      fontWeight: FontWeight.w400),
                ),
                Icon(icon),
              ],
            ),
          )),
    );
  }
}



https://gitlab.com/dj-flutter-group/food_annotation.git

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:food_annotation/config/config.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [Image.asset(Config.assets.splash_img)],
        ),
      ),
    );
  }
}


 )),
              Expanded(
                  child: CButton(
                onPressed: () {},
                buttonText: "Camera",
                textColor: Config.colors.primaryTextColor,
                buttonColor: Config.colors.secondColor,
                borderRadius: 20,
                borderColor: Colors.red,
              ))






Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bgImage.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Image.asset(
                Config.assets.sideTop,
                fit: BoxFit.cover,
              ),
              Image.asset(
                Config.assets.logo2,
                fit: BoxFit.contain,
              ),
              SizedBox(height: 30),
              Text(
                "Choose a methode",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              ),
              SizedBox(height: 30),
              CButton(
                onPressed: () => pickImage(ImageSource.camera),
                icon: Icons.camera_alt_outlined,
                buttonText: "Capture image",
                textColor: Config.colors.primaryTextColor,
                buttonColor: Config.colors.primaryColor,
                borderRadius: 10,
                borderColor: Config.colors.primaryColor,
              ),
              CButton(
                onPressed: () => pickImage(ImageSource.gallery),
                icon: Icons.image_outlined,
                buttonText: "Upload image",
                textColor: Config.colors.primaryTextColor,
                buttonColor: Config.colors.secondColor,
                borderRadius: 10,
                borderColor: Config.colors.secondColor,
              ),
              /*Image.asset(
                Config.assets.bar,
                fit: BoxFit.contain,
              ),
              SizedBox(height: 30),*/
            ],
          ),
        ),
      ),
      bottomNavigationBar: FancyBottomNavigation(
        tabs: [
          TabData(iconData: Icons.home, title: "home"),
          TabData(iconData: Icons.camera_alt, title: "camera"),
          TabData(iconData: Icons.photo_outlined, title: "My Image"),
        ],
        initialSelection: 1,
        key: bottomNavigationkey,
        onTabChangedListener: ((position) {
          setState(() {
            currentpage = position;
          });
        }),
      ),
      /*GNav(
          mainAxisAlignment: MainAxisAlignment.center,
          backgroundColor: Config.colors.primaryColor,
          color: Config.colors.primaryTextColor,
          activeColor: Config.colors.primaryTextColor,
          tabBackgroundColor: Config.colors.secondColor,
          tabs: const [
            GButton(
              icon: Icons.home,
            ),
            GButton(
              icon: Icons.camera_alt,
            ),
            GButton(icon: Icons.home),
          ],
        ),*/
    );




    import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:food_annotation/config/config.dart';
import 'package:food_annotation/widgets/button.dart';
import 'package:fancy_bottom_navigation/fancy_bottom_navigation.dart';
import 'package:image_picker/image_picker.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  Future pickImage(ImageSource camera) async {
    try {
      final image = await ImagePicker().pickImage(source: ImageSource.camera);
      if (image == null) return;

      final imageFinal = File(image.path);
      /*setState(() {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => DisplayImage(image: imageFinal)));
      });*/
    } on PlatformException catch (e) {
      print("Failed to pick image $e");
    }
  }

  int currentpage = 0;
  GlobalKey bottomNavigationkey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return OverflowBox(
      child: Scaffold(
        body: Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/bgImage.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Image.asset(
                  Config.assets.sideTop,
                  fit: BoxFit.cover,
                ),
                Image.asset(
                  Config.assets.logo2,
                  fit: BoxFit.contain,
                ),
                SizedBox(height: 30),
                Text(
                  "Choose a methode",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                ),
                SizedBox(height: 30),
                CButton(
                  onPressed: () => pickImage(ImageSource.camera),
                  icon: Icons.camera_alt_outlined,
                  buttonText: "Capture image",
                  textColor: Config.colors.primaryTextColor,
                  buttonColor: Config.colors.primaryColor,
                  borderRadius: 10,
                  borderColor: Config.colors.primaryColor,
                ),
                CButton(
                  onPressed: () => pickImage(ImageSource.gallery),
                  icon: Icons.image_outlined,
                  buttonText: "Upload image",
                  textColor: Config.colors.primaryTextColor,
                  buttonColor: Config.colors.secondColor,
                  borderRadius: 10,
                  borderColor: Config.colors.secondColor,
                ),
                /*Image.asset(
                Config.assets.bar,
                fit: BoxFit.contain,
              ),
              SizedBox(height: 30),*/
              ],
            ),
          ),
        ),
        bottomNavigationBar: FancyBottomNavigation(
          tabs: [
            TabData(iconData: Icons.home, title: "home"),
            TabData(iconData: Icons.camera_alt, title: "camera"),
            TabData(iconData: Icons.photo_outlined, title: "My Image"),
          ],
          initialSelection: 1,
          key: bottomNavigationkey,
          onTabChangedListener: ((position) {
            setState(() {
              currentpage = position;
            });
          }),
        ),
      ),
    );
  }
}

*/