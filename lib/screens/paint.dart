
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ResizebleWidget extends StatefulWidget {
  late Color? color_text;
  late double? height;
  late double? width;
  late String? label;
  late double? axe_y ;
  late double? axe_x;
  late int id;

  ResizebleWidget(
      {
        required this.id,
        this.child = const Text(""),
        Color? this.color_text,
        String? this.label,
        double? this.axe_x,
        double? this.axe_y,
        double? this.height = 200,
        double? this.width = 200,
      });

  final Widget child;




  factory ResizebleWidget.fromJson(Map<String, dynamic> json) =>
      ResizebleWidget(
        id: json["Id"] as int,
        label: json["label"]  as String,
        axe_x: json["x"] as double,
        axe_y: json["y"] as double,
        width: json["width"] as double,
        height: json["height"] as double,
        color_text: Color(json["color"] as int),
        child: const Text(""),
      );

  Map<String, dynamic> toJson() {
    return {
      "Id": this.id,
      "label": this.label,
      "x": this.axe_x,
      "y": this.axe_y ,
      "width": this.width,
      "height": this.height,
      "color": this.color_text!.value
    };
  }

  @override
  _ResizebleWidgetState createState() => _ResizebleWidgetState();
}

const ballDiameter = 30.0;

class _ResizebleWidgetState extends State<ResizebleWidget> {
  late double height;
  late double width;
  late String label;
  late double axe_y;
  late double axe_x;
  late final int id;


  get_state(){
    setState(() {
      widget.height = this.height;
      widget.width = this.width;
      widget.label = this.label;
      widget.axe_y = this.axe_y ;
      widget.axe_x = this.axe_x;
      widget.id = this.id;
    });



  }
  Color color_text = Colors.black;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    id =  (widget.id != null? widget.id:1)!;
    height = (widget.height != null? widget.height:200)!;
    width = (widget.width != null? widget.height:200)!;
    label = widget.label??'unkown';
    axe_y = widget.axe_y??0;
    axe_x = widget.axe_x??0;
    color_text = widget.color_text??Colors.black;
  }
  void onDrag(double dx, double dy) {
    var newHeight = height + dy;
    var newWidth = width + dx;

    setState(() {
      widget.toJson();
      get_state();
      height = newHeight > 0 ? newHeight : 0;
      width = newWidth > 0 ? newWidth : 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    get_state();
    return Stack(
      children: <Widget>[
        Positioned(
          top: axe_y,
          left: axe_x,
          child: Container(
            height: height,
            width: width,
            decoration: BoxDecoration(
              border: Border.all(
                  color: color_text, width: 1),
              borderRadius: BorderRadius.circular(5),
              shape: BoxShape.rectangle,
            ),

            child: widget.child,
          ),
        ),
        // axe_y axe_x
        Positioned(
          top: axe_y - ballDiameter / 2,
          left: axe_x - ballDiameter / 2,
          child: ManipulatingBall(
            onDrag: (dx, dy) {
              var mid = (dx + dy) / 2;
              var newHeight = height - 2 * mid;
              var newWidth = width - 2 * mid;

              setState(() {
                height = newHeight > 0 ? newHeight : 0;
                width = newWidth > 0 ? newWidth : 0;
                axe_y = axe_y + mid;
                axe_x = axe_x + mid;
              });
            }, key: null,
          ),
        ),
        // axe_y middle
        Positioned(
          top: axe_y - ballDiameter / 2,
          left: axe_x + width / 2 - ballDiameter / 2,
          child: ManipulatingBall(
            onDrag: (dx, dy) {
              var newHeight = height - dy;

              setState(() {
                height = newHeight > 0 ? newHeight : 0;
                axe_y = axe_y + dy;
              });
            }, key: null,
          ),
        ),
        // axe_y right
        Positioned(
          top: axe_y - ballDiameter / 2,
          left: axe_x + width - ballDiameter / 2,
          child: ManipulatingBall(
            onDrag: (dx, dy) {
              var mid = (dx + (dy * -1)) / 2;

              var newHeight = height + 2 * mid;
              var newWidth = width + 2 * mid;

              setState(() {
                height = newHeight > 0 ? newHeight : 0;
                width = newWidth > 0 ? newWidth : 0;
                axe_y = axe_y - mid;
                axe_x = axe_x - mid;
              });
            },
          ),
        ),
        // center right
        Positioned(
          top: axe_y + height / 2 - ballDiameter / 2,
          left: axe_x + width - ballDiameter / 2,
          child: ManipulatingBall(
            onDrag: (dx, dy) {
              var newWidth = width + dx;

              setState(() {
                width = newWidth > 0 ? newWidth : 0;
              });
            },
          ),
        ),
        // bottom right
        Positioned(
          top: axe_y + height - ballDiameter / 2,
          left: axe_x + width - ballDiameter / 2,
          child: ManipulatingBall(
            onDrag: (dx, dy) {
              var mid = (dx + dy) / 2;

              var newHeight = height + 2 * mid;
              var newWidth = width + 2 * mid;

              setState(() {
                height = newHeight > 0 ? newHeight : 0;
                width = newWidth > 0 ? newWidth : 0;
                axe_y = axe_y - mid;
                axe_x = axe_x - mid;
              });
            },
          ),
        ),
        // bottom center
        Positioned(
          top: axe_y + height - ballDiameter / 2,
          left: axe_x + width / 2 - ballDiameter / 2,
          child: ManipulatingBall(
            onDrag: (dx, dy) {
              var newHeight = height + dy;

              setState(() {
                height = newHeight > 0 ? newHeight : 0;
              });
            },
          ),
        ),
        Positioned(
            top: axe_y,
            left: axe_x,
            child: Container(
              padding: EdgeInsets.only(bottom: 10, left: 10, right: 10),
              color: color_text,
              child: Text(
                label,
                style: TextStyle(color:Colors.white , fontSize: 12),
              ),
            )),
        // bottom axe_x
        Positioned(
          top: axe_y + height - ballDiameter / 2,
          left: axe_x - ballDiameter / 2,
          child: ManipulatingBall(
            onDrag: (dx, dy) {
              var mid = ((dx * -1) + dy) / 2;

              var newHeight = height + 2 * mid;
              var newWidth = width + 2 * mid;

              setState(() {
                height = newHeight > 0 ? newHeight : 0;
                width = newWidth > 0 ? newWidth : 0;
                axe_y = axe_y - mid;
                axe_x = axe_x - mid;
              });
            },
          ),
        ),
        //axe_x center
        Positioned(
          top: axe_y + height / 2 - ballDiameter / 2,
          left: axe_x - ballDiameter / 2,
          child: ManipulatingBall(
            onDrag: (dx, dy) {
              var newWidth = width - dx;

              setState(() {
                width = newWidth > 0 ? newWidth : 0;
                axe_x = axe_x + dx;
              });
            },
          ),
        ),
        // center center
        Positioned(
          top: axe_y + height / 2 - ballDiameter / 2,
          left: axe_x + width / 2 - ballDiameter / 2,
          child: ManipulatingBall(
            onDrag: (dx, dy) {
              setState(() {
                axe_y = axe_y + dy;
                axe_x = axe_x + dx;
              });
            }, key: null,
          ),
        ),
      ],
    );
  }


}

class ManipulatingBall extends StatefulWidget {
  ManipulatingBall({Key? key, required this.onDrag});

  final Function onDrag;

  @override
  _ManipulatingBallState createState() => _ManipulatingBallState();
}

class _ManipulatingBallState extends State<ManipulatingBall> {
  late double initX;
  late double initY;

  _handleDrag(details) {
    setState(() {
      initX = details.globalPosition.dx;
      initY = details.globalPosition.dy;
    });
  }

  _handleUpdate(details) {
    var dx = details.globalPosition.dx - initX;
    var dy = details.globalPosition.dy - initY;
    initX = details.globalPosition.dx;
    initY = details.globalPosition.dy;
    widget.onDrag(dx, dy);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onPanStart: _handleDrag,
      onPanUpdate: _handleUpdate,
      child: Container(
        width: ballDiameter,
        height: ballDiameter,
        decoration: BoxDecoration(
          color: Colors.blue.withOpacity(0.5),
          shape: BoxShape.circle,
        ),
      ),
    );
  }
}
