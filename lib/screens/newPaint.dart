import 'dart:math';
import 'dart:io';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';

import 'package:food_annotation/screens/annotation_files.dart';
import 'package:food_annotation/screens/paint.dart';
import 'package:food_annotation/screens/storage.dart';



class Demo extends StatefulWidget {
  @override
  _DemoState createState() => _DemoState();
}

class _DemoState extends State<Demo> {
  List<ResizebleWidget> anotation_widget = [];
  List labels_list = [];
  List labelList = [];
  late TextEditingController controler_test;

  late String label_name;
  final _myboxImage = Hive.box('recent_image');

  late Color selectColor;
  Map<String, dynamic> data = {};
  Labels fileToSave = Labels("");
  File bgImageAnnotation = File("assets/images/bgImage.png");
  bool load = false;
  @override
  initState() {
    // TODO: implement initState
    controler_test = TextEditingController();
    label_name = " ";

    selectColor = Colors.red;

    super.initState();
  }

  void readAsync(String filename) async {
    Labels fileToSave = Labels(filename);

    if (!load){
      Annotations? data = await fileToSave.readJsonFile();
      read(data);
      load = true;
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    controler_test.dispose();
    super.dispose();
  }

  void save() {
    // List<Map<String,dynamic>> data = [];
    // for (var element in anotation_widget) {
    //     data.add(element.toJson());
    // }
    Annotations dataAnnotation =
        Annotations(image: data["image"].path, annotations: anotation_widget);
    saveImage(data["image"].path);
    fileToSave = Labels(dataAnnotation.image);
    fileToSave.writeJsonFile(dataAnnotation);
  }
  void saveImage(String imgPath){
    _myboxImage.add(imgPath);
  }


  void read(Annotations? data) {
    // Annotations
    print("test de position");
    bgImageAnnotation = File((data?.image)!);
    List<ResizebleWidget> annotationLoad = (data?.annotations)!;
    // for(Map<String,dynamic> element in annotationLoad){
    //   ResizebleWidget data = ResizebleWidget.fromJson(element);
    //
    //   anotationLoad.add(data);
    // }

    setState(() {
      anotation_widget = annotationLoad;
    });
  }

  @override
  Widget build(BuildContext context) {
    data = ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>;
    bgImageAnnotation = data["image"];
    print(bgImageAnnotation.path.split('/').last);
    readAsync(bgImageAnnotation.path.split('/').last);
    return Scaffold(
      //appBar: AppBar(title: Text('Food Anotation'),),
      body: Stack(
        children: [

          Container(
            decoration:  BoxDecoration(
              image:  DecorationImage(
                image: FileImage(bgImageAnnotation) as ImageProvider,
                fit: BoxFit.cover,
              ),
            ),
            child: Stack(
              children: anotation_widget,
            ),
          ),
        ],
      ),
      floatingActionButton: SpeedDial(
        icon: Icons.menu,
        //icon on Floating action button
        activeIcon: Icons.close,
        //icon when menu is expanded on button
        backgroundColor: Colors.deepOrangeAccent,
        //background color of button
        foregroundColor: Colors.white,
        //font color, icon color in button
        activeBackgroundColor: Colors.deepPurpleAccent,
        //background color when menu is expanded
        activeForegroundColor: Colors.white,
        visible: true,
        closeManually: false,
        curve: Curves.bounceIn,
        overlayColor: Colors.black,
        overlayOpacity: 0.5,
        onOpen: () => print('OPENING DIAL'),
        // action when menu opens
        onClose: () => print('DIAL CLOSED'),
        //action when menu closes

        elevation: 8.0,
        //shadow elevation of button
        shape: CircleBorder(),
        //shape of button

        children: [
          SpeedDialChild(
            //speed dial child
            child: Icon(Icons.list_alt),
            backgroundColor: Colors.red,
            foregroundColor: Colors.white,
            label: 'list of labels',
            labelStyle: TextStyle(fontSize: 18.0),
            onTap: () async {
              final name = await ListlabelsDialog();
              if (name == null || name.isEmpty) return;
              setState(() => label_name = name);
              addframe();
            },
            onLongPress: () => print('FIRST CHILD LONG PRESS'),
          ),
          SpeedDialChild(
            child: Icon(Icons.save),
            backgroundColor: Colors.blue,
            foregroundColor: Colors.white,
            label: 'Save your works',
            labelStyle: TextStyle(fontSize: 18.0),
            onTap: () => save(),
            onLongPress: () => print('SECOND CHILD LONG PRESS'),
          ),
          SpeedDialChild(
            child: Icon(Icons.add),
            foregroundColor: Colors.white,
            backgroundColor: Colors.green,
            label: 'add label',
            labelStyle: TextStyle(fontSize: 18.0),
            onTap: () async {
              final name = await openDialog();
              if (name == null || name.isEmpty) return;
              setState(() => label_name = name);
              addframe();
            },
            onLongPress: () => print('THIRD CHILD LONG PRESS'),
          ),

          SpeedDialChild(
            child: Icon(Icons.exit_to_app),
            foregroundColor: Colors.white,
            backgroundColor: Colors.green,
            label: 'EXIT',
            labelStyle: TextStyle(fontSize: 18.0),
            onTap: () async {
              Navigator.of(context).pushReplacementNamed('/home');
            },
            onLongPress: () => print('THIRD CHILD LONG PRESS'),
          ),
          //add more menu item children here
        ],
      ),
      // FloatingActionButton(
      //   backgroundColor: Colors.black,
      //   child: const Icon(Icons.add),
      //   onPressed: () async {
      //
      //     final name = await openDialog();
      //
      //     if (name == null || name.isEmpty) return;
      //     setState(() => label_name = name);
      //     addframe();
      //
      // },
      // ),
    );
  }

  Future<String?> openDialog() => showDialog<String>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
            title: Text("Name of the Label "),
            content: SizedBox(
              width: double.maxFinite,
              child: Expanded(
                child: ListView(
                  shrinkWrap: true,
                  children: [
                    TextField(
                      decoration: const InputDecoration(
                        hintText: "Enter the Name of the label",
                        hintStyle: TextStyle(fontSize: 14),
                        border: OutlineInputBorder(), // <-- This is the key
                        labelText: "Enter the Name of the label",
                      ),
                      controller: controler_test,
                    ),
                    const SizedBox(height: 10),
                  ],
                ),
              ),
            ),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop(controler_test.text);
                    controler_test.text = "";
                  },
                  child: Text("Submit"))
            ],
          )
      // openDialog();

      );

  Future<String?> ListlabelsDialog() => showDialog<String>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
          title: const Text("The List of all Label "),
          content: SizedBox(
            width: double.maxFinite,
            child: Expanded(
              child: listOfLabel(),
            ),
          ))
      // openDialog();

      );

  Future<void> addframe() async {
    setState(() async {
      var tmp = ResizebleWidget(
        id: anotation_widget.length + 1,
        label: label_name,
        color_text: randomColor(),
        axe_y: 0,
        axe_x: 0,
      );
      anotation_widget.add(tmp);
    });
  }

  Widget selectLabel() {
    return ListView.builder(
      // Let the ListView know how many items it needs to build.
      itemCount: anotation_widget.length,
      // Provide a builder function. This is where the magic happens.
      // Convert each item into a widget based on the type of item it is.
      itemBuilder: (context, index) {
        final ResizebleWidget item = anotation_widget[index];
        return Card(
          child: ListTile(
            onTap: () {
              setState(() async {
                controler_test.text = item.label!;
                Navigator.of(context).pop();
              });
            },
            title: Text(item.label!),
            trailing: const Icon(Icons.add),
          ),
        );
      },
    );
  }

  Color randomColor() {
    Color color = Color.fromARGB(255, Random().nextInt(256),
        Random().nextInt(256), Random().nextInt(256));
    return color;
  }

  Widget listOfLabel() {

    return ListView.builder(
      // Let the ListView know how many items it needs to build.
      itemCount: anotation_widget.length,
      // Provide a builder function. This is where the magic happens.
      // Convert each item into a widget based on the type of item it is.
      itemBuilder: (context, index) {
        final ResizebleWidget item = anotation_widget[index];
        return Card(
          child: ListTile(
            onTap: () {
              setState(() {
                anotation_widget.remove(item);
                Navigator.of(context).pop();
              });
            },
            title: Text(item.label!),
            trailing: const Icon(Icons.delete),
          ),
        );
      },
    );
  }
}
