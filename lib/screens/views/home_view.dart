import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:food_annotation/config/config.dart';
import 'package:food_annotation/widgets/button.dart';
import 'package:image_picker/image_picker.dart';

class CameraPage extends StatefulWidget {
  const CameraPage({super.key});

  @override
  State<CameraPage> createState() => _CameraPageState();
}

class _CameraPageState extends State<CameraPage> {
  Future pickImage(ImageSource source, BuildContext context) async {
    try {
      final image = await ImagePicker().pickImage(source: source);
      if (image == null) return;
      final imageFinal = await saveImagePermanently(image.path);
      setState(() {
        Navigator.pushReplacementNamed(context, '/paint', arguments: {
          'image': imageFinal,
        });
      });
    } on PlatformException catch (e) {
      print("Failed to pick image $e");
    }
  }

  Future<File> saveImagePermanently(String imagePath) async {
    print(imagePath);

    return File(imagePath);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // color: Colors.deepPurple[100],
      // appBar: AppBar(
      //   title: Text("First"),
      // ),
      body: Container(
        margin: const EdgeInsets.all(0),
        width: double.infinity,
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bgImage.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Expanded(
                  child: ListView(
                shrinkWrap: true,
                children: <Widget>[
                  Image.asset(
                    Config.assets.sideTop,
                    fit: BoxFit.cover,
                  ),
                  Image.asset(
                    Config.assets.logo2,
                    fit: BoxFit.scaleDown,
                  ),
                  const SizedBox(height: 10),
                  const Center(
                    child: Text(
                      "Choose a methode",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                  ),
                  const SizedBox(height: 10),
                  CButton(
                    onPressed: () => pickImage(ImageSource.camera, context),
                    icon: Icons.camera_alt_outlined,
                    buttonText: "Capture image",
                    textColor: Config.colors.primaryTextColor,
                    buttonColor: Config.colors.primaryColor,
                    borderRadius: 10,
                    borderColor: Config.colors.primaryColor,
                  ),
                  CButton(
                    onPressed: () => pickImage(ImageSource.gallery, context),
                    icon: Icons.image_outlined,
                    buttonText: "Upload image",
                    textColor: Config.colors.primaryTextColor,
                    buttonColor: Config.colors.secondColor,
                    borderRadius: 10,
                    borderColor: Config.colors.secondColor,
                  ),
                ],
              ))
            ],
          ),
        ),
      ),
    );
  }
}
