import 'package:flutter/material.dart';

import 'package:hive_flutter/hive_flutter.dart';

import '../CardImg.dart';


class List_View extends StatefulWidget {
  const List_View({super.key});

  @override
  State<List_View> createState() => _List_ViewState();
}

class _List_ViewState extends State<List_View> {
  final _myboxImage = Hive.box('recent_image');
  List<Widget> imageList = [];
  @override
  void initState() {
    loadImage();
    super.initState();
  }
  loadImage(){
    List<Widget> imageListTmp = [];
    for(int i = 0; i<_myboxImage.length; i++){
      imageListTmp.add(ImageCard(file:_myboxImage.get(i)));
    }
    if (imageListTmp.isEmpty){
      print("is empty");
      imageListTmp.add(const ImageCard(file:"assets/images/bgImage.png"));
    }

    setState(() {
        imageList = imageListTmp;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(
          child: ListView(
            shrinkWrap: true,
            children: imageList,
          ),
        ),]
      ),
    );
  }
}
