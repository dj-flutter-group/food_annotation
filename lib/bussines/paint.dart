import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:food_annotation/bussines/paintonImage.dart';


import '../Entity/drawingarea.dart';


class PaintPage extends StatefulWidget {
  const PaintPage({Key? key}) : super(key: key);

  @override
  State<PaintPage> createState() => _PaintPageState();
}

class _PaintPageState extends State<PaintPage> {
  List<DrawingArea?> points = [];
  Color selectedColor = Colors.black;
  double strokeWidth = 2;
  DrawingArea? debut = null;
  DrawingArea? fin = null;

  String image_path = "assets/download.jpeg" ;

  @override
  void initState(){
    super.initState();
    selectedColor = Colors.black;
    strokeWidth = 2.0;


  }


  void selectColor(){
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Scaffold(
          backgroundColor: Colors.black.withOpacity(0.0),
          body: Center(

            child: AlertDialog(
                title: Text('Choose your Color'),
                content: SingleChildScrollView(
                  child: BlockPicker(
                    pickerColor: selectedColor,
                    onColorChanged: (color){
                      this.setState(() {
                        selectedColor = color;
                      });
                    },
                  ),
                ),
                actions: <Widget> [
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text('close')),
                ]
            ),

          ),
        );
      },


    );
  }
  @override
  Widget build(BuildContext context) {
    image_path = getImage(context)!;
    final double width = MediaQuery.of(context).size.width;
    final double heigth = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Stack(
        children: <Widget> [
          Container(

            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      Color.fromRGBO(66, 172, 5, 1.0),
                      Color.fromRGBO(241, 194, 6, 1.0),
                      Color.fromRGBO(55, 133, 3, 1.0),
                    ]
                )
            ),
          ),
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(

                  width: width*1,
                  height: heigth*0.80,
                  decoration: BoxDecoration(
                    // borderRadius: BorderRadius.all(Radius.circular(20)),
                    image: DecorationImage(
                        image: AssetImage(image_path),
                        fit: BoxFit.cover),
                  ),
                  child: GestureDetector(
                    onPanDown:(details){
                      setState(() {
                        DrawingArea draw = DrawingArea(
                            point: details.localPosition,
                            areaPoint: Paint()..strokeCap = StrokeCap.round
                              ..isAntiAlias = true
                              ..color = selectedColor
                              ..strokeWidth = strokeWidth
                        );
                        points.add(draw);
                        debut = draw;

                        print('hello world : ${debut!.point}');
                      });
                    } ,
                    onPanUpdate: (details){
                      setState(() {
                        DrawingArea draw = DrawingArea(
                            point: details.localPosition,
                            areaPoint: Paint()..strokeCap = StrokeCap.round
                              ..isAntiAlias = true
                              ..color = selectedColor
                              ..strokeWidth = strokeWidth
                        );
                        points.add(draw);
                      });
                    },
                    onPanEnd: (details){
                      fin = points[points.length-1]!;
                      print('hello world end: ${fin!.point}');
                      print('all the label point \n ${points.getRange(
                          points.indexOf(debut),
                          points.indexOf(fin))}');
                      points.add(null);


                    },
                    child: ClipRRect(
                      borderRadius: const BorderRadius.all(Radius.circular(20)),
                      child: CustomPaint(
                        painter: PainterOnImage(points: points),
                      ),
                    ),
                  ),
                ),

                Column(
                  children: <Widget>[
                    Container(

                      decoration: BoxDecoration(
                        color: Colors.white,

                      ),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            flex: 1,
                            child: IconButton(onPressed: (){
                              selectColor();
                            }, icon: Icon(
                              Icons.color_lens,
                              color: selectedColor,
                            )),
                          ),
                          Expanded(
                              flex: 2,
                              child: Slider(
                                min: 1.0,
                                max: 10.0,
                                activeColor: selectedColor,
                                value: strokeWidth, onChanged: (value){
                                this.setState(() {
                                  strokeWidth = value;
                                });
                              },
                              )),
                          Expanded(
                            child: IconButton(onPressed: (){
                              selectColor();
                            }, icon: Icon(
                                Icons.save
                            )),
                          ),
                          Expanded(
                            flex:1,
                            child: IconButton(
                              onPressed: (){points.clear();}, icon: Icon(
                                Icons.layers_clear
                            ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                    padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
                      decoration: BoxDecoration(

                        color: Colors.white,
                        border: Border.symmetric(vertical: BorderSide.none),
                      ),

                      child: Center(
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              flex: 1,
                              child: IconButton(onPressed: (){
                                selectColor();
                              }, icon: Icon(
                                Icons.import_export_sharp,

                              )),
                            ),

                            Expanded(
                              flex: 1,
                              child: IconButton(onPressed: (){
                                selectColor();
                              }, icon: Icon(
                                  Icons.list
                              )),
                            ),
                            Expanded(
                              flex: 1,
                              child: IconButton(onPressed: (){
                                // Navigator.pop(context, );
                              }, icon: Icon(
                                Icons.arrow_back,
                              )),
                            ),

                          ],
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  String? getImage(BuildContext context) {
    var args = ModalRoute.of(context)?.settings.arguments;
    return image_path;
  }
}

