import 'dart:ui';

import 'package:flutter/material.dart';

import '../Entity/drawingarea.dart';


class PainterOnImage extends CustomPainter{
  List<DrawingArea?> points;
  PainterOnImage({required this.points});
  @override
  void paint(Canvas canvas, Size size) {
    Paint background = Paint()..color = Colors.transparent;
    Rect rect = Rect.fromLTWH(0, 0, size.width, size.height);
    canvas.drawRect(rect, background);


    for(int i=0; i< points.length-1; i++){

      if (points[i] != null && points[i+1] != null){
        Offset x = points[i]!.point;
        Offset y = points[i+1]!.point;

        canvas.drawLine(x, y, points[i]!.areaPoint);
      }
      else if(points[i] != null && points[i+1] == null){
        Offset x = points[i]!.point;
        canvas.drawPoints(PointMode.points, [x], points[i]!.areaPoint);
      }
    }

  }

  @override
  bool shouldRepaint(PainterOnImage oldDelegate) {
    return true;
  }

}