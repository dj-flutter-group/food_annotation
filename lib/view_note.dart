// import 'package:flutter/cupertino.dart';
// import 'package:flutter/gestures.dart';
// import 'package:flutter/material.dart';
// import 'package:food_annotation/notes_databases.dart';
// import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
// import 'package:intl/intl.dart';
// import 'model/notes.dart';
//
// class Notespage extends StatefulWidget {
//   const Notespage({Key? key}) : super(key: key);
//
//   @override
//   State<Notespage> createState() => _NotespageState();
// }
//
// class _NotespageState extends State<Notespage> {
//   late List<Note> notes = [];
//   bool isLoading = false;
//
//   @override
//   void initState() {
//     super.initState();
//     refreshNotes();
//   }
//
//   @override
//   void dispose() {
//     NotesDatabase.instance.close();
//     super.dispose();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text(
//           'Notes',
//           style: TextStyle(fontSize: 24),
//         ),
//         actions: [Icon(Icons.search), SizedBox(width: 12)],
//       ),
//       body: Center(
//         child: isLoading
//             ? CircularProgressIndicator()
//             : notes.isEmpty
//                 ? Text('No Notes',
//                     style: TextStyle(
//                       color: Colors.white,
//                     ))
//                 : buildNotes(),
//       ),
//       floatingActionButton: FloatingActionButton(
//         backgroundColor: Colors.black,
//         child: Icon(Icons.add),
//         onPressed: () =>{
//
//         },
//       ),
//     );
//   }
//
//   Widget buildNotes() => StaggeredGrid.count(
//         crossAxisCount: 4,
//         mainAxisSpacing: 4,
//         children: [
//           ListView.builder(itemBuilder: (context, index) {
//             final note = notes[index];
//             return GestureDetector(
//               onTap: () async {
//                 await Navigator.of(context).push(MaterialPageRoute(
//                     builder: (context) => NoteDetailPage(noteId: (note.id)!)));
//               },
//             );
//           })
//         ],
//       );
//
//   Future refreshNotes() async {
//     this.notes = await NotesDatabase.instance.readAllNote();
//     setState(() {
//       isLoading = false;
//     });
//   }
// }
//
// class NoteDetailPage extends StatefulWidget {
//   final int noteId;
//
//   const NoteDetailPage({Key? key, required this.noteId}) : super(key: key);
//
//   @override
//   State<NoteDetailPage> createState() => _NoteDetailPageState();
// }
//
// class _NoteDetailPageState extends State<NoteDetailPage> {
//   late Note note;
//   bool isloading = false;
//
//   @override
//   void initState() {
//     super.initState();
//     refreshNotes();
//   }
//
//   Future refreshNotes() async {
//     this.note = (await NotesDatabase.instance.readNote(widget.noteId))!;
//     setState(() {
//       isloading = false;
//     });
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         actions: [editButton(), deleteButton()],
//       ),
//       body: isloading
//           ? Center(
//               child: CircularProgressIndicator(),
//             )
//           : Padding(
//               padding: EdgeInsets.all(12),
//               child: ListView(
//                 padding: EdgeInsets.symmetric(vertical: 8),
//                 children: [
//                   Text(
//                     note.title,
//                     style: TextStyle(
//                       color: Colors.white,
//                       fontSize: 32,
//                       fontWeight: FontWeight.bold,
//                     ),
//                   ),
//                   SizedBox(height: 8),
//                   Text(DateFormat.yMMMd().format(note.createTime),
//                       style: TextStyle(color: Colors.white38)),
//                   SizedBox(height: 8),
//                   Text(
//                     note.description,
//                     style: TextStyle(
//                       color: Colors.white70,
//                       fontSize: 18,
//                     ),
//                   ),
//                 ],
//               ),
//             ),
//     );
//   }
//
//   Widget editButton() => IconButton(
//         icon: Icon(Icons.edit_outlined),
//         onPressed: () async {
//           if (isloading) return;
//
//           await Navigator.of(context).push(MaterialPageRoute(
//               builder: (context) => AddEditNotePage(note: note)));
//         },
//       );
//
//   deleteButton() => IconButton(
//         icon: Icon(Icons.delete),
//         onPressed: () async {
//           await NotesDatabase.instance.delete(widget.noteId);
//           Navigator.of(context).pop();
//         },
//       );
// }
//
// class AddEditNotePage extends StatefulWidget {
//   final Note? note;
//
//   const AddEditNotePage({Key? key, this.note}) : super(key: key);
//
//   @override
//   State<AddEditNotePage> createState() => _AddEditNotePageState();
// }
//
// class _AddEditNotePageState extends State<AddEditNotePage> {
//   final _formKey = GlobalKey<FormState>();
//   late int? id;
//   late bool isImportant;
//   late int number;
//   late String title;
//   late String description;
//   late DateTime createTime;
//
//   void AddorUpdateNote() async {
//     final isvalid = _formKey.currentState!.validate();
//     if (isvalid) {
//       final isUpdating = widget.note != null;
//       if (isUpdating) {
//         await updateNote();
//       } else {
//         await addNote();
//       }
//       Navigator.of(context).pop();
//     }
//   }
//
//   Future updateNote() async {
//     final note = widget.note!.copy(
//         isImportant: isImportant,
//         number: number,
//         title: title,
//         description: description);
//     await NotesDatabase.instance.update(note);
//   }
//
//   Future addNote() async {
//     final note = Note(
//         createTime: DateTime.now(),
//         isImportant: isImportant,
//         number: number,
//         title: title,
//         description: description);
//     await NotesDatabase.instance.create(note);
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Container();
//   }
// }
//
// class FormStates {}
//
// class NoteCardWidget extends StatefulWidget {
//   const NoteCardWidget({Key? key}) : super(key: key);
//
//   @override
//   State<NoteCardWidget> createState() => _NoteCardWidgetState();
// }
//
// class _NoteCardWidgetState extends State<NoteCardWidget> {
//   @override
//   Widget build(BuildContext context) {
//     return Container();
//   }
// }
