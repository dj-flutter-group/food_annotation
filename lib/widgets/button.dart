import 'package:flutter/material.dart';
import 'package:food_annotation/config/config.dart';

class CButton extends StatelessWidget {
  VoidCallback onPressed;
  Color? buttonColor;
  double? borderRadius;
  Color? textColor;
  IconData icon;
  String buttonText;
  Color? borderColor;

  CButton({
    required this.onPressed,
    this.borderRadius,
    this.borderColor,
    this.buttonColor,
    this.textColor,
    required this.icon,
    required this.buttonText,
    Key? key,
  }) : super(key: key);

  @override
  /*Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.symmetric(horizontal: 30, vertical: 15),
        decoration: BoxDecoration(
            color: buttonColor ?? Config.colors.primaryColor,
            border: Border.all(
              color: borderColor ?? Config.colors.secondColor,
            ),
            borderRadius: BorderRadius.circular(borderRadius ?? 0)),
        /*child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: Colors.transparent,
            onPrimary: Colors.transparent,
          ),
          onPressed: onPressed,
          child: Row(
            children: [
              Text(
                buttonText,
                style: TextStyle(
                    color: textColor,
                    fontSize: 25,
                    fontWeight: FontWeight.w400),
              ),
              Icon(icon),
            ],
          ),
        )*/);
  }
}*/

  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 30, vertical: 15),
      decoration: BoxDecoration(
          color: buttonColor ?? Config.colors.primaryColor,
          border: Border.all(
            color: borderColor ?? Config.colors.secondColor,
          ),
          borderRadius: BorderRadius.circular(borderRadius ?? 0)),
      child: Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: onPressed,
            child: Container(
                padding: const EdgeInsets.all(10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text(
                      buttonText,
                      style: TextStyle(
                          color: textColor,
                          fontSize: 20,
                          fontWeight: FontWeight.w400),
                    ),
                    Icon(
                      icon,
                      size: 30,
                      color: Colors.white,
                    ),
                  ],
                )),
            /*child: Row(
              
              /*children: [
                Text(
                  buttonText,
                  style: TextStyle(
                      color: textColor,
                      fontSize: 25,
                      fontWeight: FontWeight.w400),
                ),
                Icon(
                  icon,
                  color: Colors.white,
                ),
              ],*/
            ),*/
          )),
    );
  }
}

/*Widget build(BuildContext context) {
  return Container(
    margin: const EdgeInsets.symmetric(horizontal: 30, vertical: 15),
    decoration: BoxDecoration(
        color: buttonColor ?? Config.colors.primaryColor,
        border: Border.all(
          color: borderColor ?? Config.colors.secondColor,
        ),
        borderRadius: BorderRadius.circular(borderRadius ?? 0)),
    child: Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: onPressed,
        child: Container(
          //margin: const EdgeInsets.all(50),
          padding: const EdgeInsets.all(10),
          child: Center(
              child: Text(
            buttonText,
            style: TextStyle(
                color: textColor, fontSize: 25, fontWeight: FontWeight.w400),
          )),
        ),
      ),
    ),
  );
}*/
