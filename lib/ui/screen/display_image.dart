import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:food_annotation/src/lib/image_painter.dart';

import '../../services/storage.dart';

class DisplayImage extends StatefulWidget {
  DisplayImage({Key? key, required this.image}) : super(key: key);

  File? image;

  @override
  State<DisplayImage> createState() => _DisplayImageState();
}

class _DisplayImageState extends State<DisplayImage> {
  final _imageKey = GlobalKey<ImagePainterState>();
  final _formKey = GlobalKey<FormState>();
  AlertDialog _registerDialog(BuildContext context) {
    return AlertDialog(
      title: Text(
        "Name the selection",
      ),
      content: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextFormField(
              // The validator receives the text that the user has entered.
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter anotation name';
                }
                return null;
              },
            ),
            TextFormField(
              // The validator receives the text that the user has entered.
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter the filename ';
                }
                return null;
              },
            ),
          ],
        ),
      ),
      // a widget with return your form for register a food
      actions: [
        TextButton(
          onPressed: () {
            final state = _imageKey.currentState!;
            //remove last
            state.paintHistory = state.paintHistory;
            Navigator.pop(context);
          },
          child: Text(
            "Cancel",
          ),
        ),
        TextButton(
          onPressed: () {
            if (_formKey.currentState!.validate()) {
              final state = _imageKey.currentState!;
              _formKey.currentState!
                  .save(); //_formkey is the key of your register form
              PaintInfo paintInfo = state.paintHistory.last;
              String data = "${paintInfo.toJson()}";
              print(json.encode(state.paintHistory.last
                  .toJson())); //print the path of current annotation in console
              Storage1 storage = Storage1(fileName: "test", data: data);
              storage.write();
              Navigator.pop(context);
            }
          },
          child: Text(
            "Done",
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            padding: EdgeInsets.all(10),
            child: ImagePainter.file(
              widget.image!,
              key: _imageKey,
              scalable: true,
              textDelegate: TextDelegate(),
              initialPaintMode: PaintMode.freeStyle,
              onDrawingEnd: _registerDialog(context),
            )));
  }
}

class FoodRegisterForm extends StatefulWidget {
  const FoodRegisterForm({Key? key}) : super(key: key);

  @override
  State<FoodRegisterForm> createState() => _FoodRegisterFormState();
}

class _FoodRegisterFormState extends State<FoodRegisterForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Container(
        width: 20,
        height: 2,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextFormField(
              // The validator receives the text that the user has entered.
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter some text';
                }
                return null;
              },
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: ElevatedButton(
                onPressed: () {
                  // Validate returns true if the form is valid, or false otherwise.
                  if (_formKey.currentState!.validate()) {
                    // If the form is valid, display a snackbar. In the real world,
                    // you'd often call a server or save the information in a database.
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(content: Text('Processing Data')),
                    );
                  }
                },
                child: const Text('Submit'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
