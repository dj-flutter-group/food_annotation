// import 'package:path/path.dart';
//
// import 'package:sqflite/sqflite.dart';
//
// import 'model/notes.dart';
//
// class NotesDatabase{
//    static final NotesDatabase instance = NotesDatabase._init();
//    static Database? _database;
//   NotesDatabase._init();
//
//   Future<Database> get database async {
//          if (_database != null) return _database!;
//          _database = await _initDb('notes.db');
//          return _database!;
//   }
//
//
//
//
//   Future _createDb(Database db, int version) async {
//     final String integertype = "INTEGER PRIMARY KEY AUTOINCREMENT";
//     final String booleantype = "BOOLEAN NOT NULL";
//     final String numbertype = "INTEGER NOT NULL";
//     final String varchartype = "VARCHAR(255) NOT NULL ";
//     final String TextType = "TEXT NOT NULL";
//
//     await db.execute('''
//         CREATE TABLE $TableNotes (
//             ${NoteFields.id} $integertype,
//             ${NoteFields.isImportant} $booleantype,
//             ${NoteFields.number} $numbertype,
//             ${NoteFields.title} $TextType,
//             ${NoteFields.description} $TextType,
//             ${NoteFields.createTime} $varchartype,
//         )
//       ''');
//
//   }
//
//    Future<Database> _initDb(String filePath) async {
//      final dbPath = await getDatabasesPath();
//      final path = join(dbPath, filePath);
//      return await openDatabase(path, version: 1, onCreate: _createDb);
//    }
//
//   Future <Note?> readNote(int id) async {
//       final  db = await instance.database;
//       final maps = await db.query(TableNotes,
//       columns: NoteFields.column,
//         where: '${NoteFields.id} = ?',
//         whereArgs: [id],
//
//       );
//       if (maps.isNotEmpty){
//         return Note.fromJson(maps.first);
//       }
//       return null;
//   }
//
//    Future <List<Note>> readAllNote() async {
//      final  db = await instance.database;
//      final orderBy = '${NoteFields.createTime} ASC';
//      final result = await db.query(TableNotes, columns: NoteFields.column,
//      orderBy: orderBy);
//
//        return result.map((json) => Note.fromJson(json)).toList();
//    }
//
//    Future <int> update(Note note) async {
//      final  db = await instance.database;
//
//
//      return db.update(
//          TableNotes,
//          note.toJson(),
//          where: '${NoteFields.id} = ? ',
//          whereArgs: [note.id],
//          );
//
//    }
//
//    Future <int> delete(int id) async {
//      final  db = await instance.database;
//
//
//      return db.delete(
//        TableNotes,
//        where: '${NoteFields.id} = ? ',
//        whereArgs: [id],
//      );
//
//    }
//
//
//
//
//    Future<Note> create(Note note) async{
//     final db = await instance.database;
//     final id = await db.insert(TableNotes, note.toJson());
//     return note.copy(id:id);
//   }
//
//   Future close() async {
//     final db = await instance.database;
//     db.close();
//   }
// }